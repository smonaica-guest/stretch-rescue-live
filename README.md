__Build Instructions__

Ideally, use Debian Stable (currently v.10.0/"Buster"), as the build host.
Building on Debian Testing or Unstable ("Sid") should also work.

Ensure you have `sudo` capability to run programs as root.

Install the following packages:
  - `live-build`
  - `apt-cacher-ng`

Create `/etc/live/build.conf` with the following content:

```
LB_APT_HTTP_PROXY="http://localhost:3142/"
```

If you have at least 8 GB of RAM available, mount a `tmpfs`
of size 8 GB (or larger) on which to run the build. E.g.:

```
mkdir -p "$HOME/mybuild"
sudo mount -t tmpfs -o size=8G tmpfs "$HOME/mybuild"
```

Clone the project into the build directory. E.g.:

```
mkdir -p "$HOME/mybuild"
cd "$HOME/mybuild"
git clone https://gitlab.sd57.bc.ca/smonaica/stretch-rescue-live.git
```

Run the build script:

```
cd "$HOME/mybuild/stretch-rescue-live/"
sudo http_proxy="http://localhost:3142/" ./make.sh
```

Presuming the build succeeds, a bootable ISO file is created that
can be "burned" to a USB stick, either via `dd` or with a GUI tool.

E.g.: using `dd` to "burn" to a USB drive, deleting the drive's
existing contents (replace `/dev/sdX` with the actual USB device name):

```
sudo dd if=stretch-rescue-live-*.iso of=/dev/sdX bs=4M status=progress
```

The ISO file can also be used in a virtual CD/DVD drive to boot a VM.

